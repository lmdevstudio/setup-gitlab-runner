<h1>Runner Registration Instructions</h1>
<h2>1. Obtain the Runner Registration Token</h2>
<p>Go to your GitLab project:</p>
<ol>
  <li>Navigate to: Settings &gt; CI/CD</li>
  <li>Expand the 'Runners' section</li>
  <li>Click on "New project runner"</li>
</ol>

<h2>2. Run the following Docker commands</h2>
<pre><code>
  docker run -d \
    --name gitlab-runner \
    --restart always \
    -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest
</code></pre>
<pre><code>
  docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
    --non-interactive \
    --docker-privileged \
    --docker-volumes "/certs/client" \
    --executor docker \
    --docker-image "docker" \
    --url https://gitlab.com/ \
    --registration-token &lt;REGISTRATION_TOKEN&gt;
</code></pre>

<p>Make sure to replace <code>&lt;REGISTRATION_TOKEN&gt;</code> with the actual registration token obtained from the first step.</p>
<p>Change also <pre><code>--url https://gitlab.com/</code></pre> with your gitlab url if you are self-hosting GitLab.

<h2>3. Example Usage</h2>
<pre><code>
  build:
    image: docker:20.10.16-dind
    services:
      - name: docker:20.10.16-dind
        alias: docker
    variables:
      CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    before_script:
      - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    script:
      - docker build -t $CONTAINER_RELEASE_IMAGE .
      - docker push $CONTAINER_RELEASE_IMAGE
    only:
      - tags
</code></pre>